let icon = document.querySelector (".menu-icon");

icon.addEventListener ("click", () => {
    if (!icon.classList.contains("open")) {
        icon.classList.add("open");
        icon.classList.remove("close");
        console.log(icon);
    } else {
        icon.classList.add("close");
        icon.classList.remove("open");
        console.log(icon);
    }
});